int turn = -1;
int[][] boardx = new int[3][3];
int[][] boardy = new int[3][3];
int[][] board = new int[3][3];
int spacesLeft = 9;
boolean isGameFinished;
PFont f;

void setup() {
  size(300, 300);
  background(0, 0, 255);
  
  f = createFont("Arial",16,true); 
  
  stroke(0);
  line(0,100,400,100);
  line(0,200,400,200);
  line(100,0,100,400);
  line(200,0,200,400);
  
  for (int i=0; i<3; i++)
  {
    for (int j=0; j<3; j++)
    {
      boardx[i][j] = i*100 + 50;
      boardy[i][j] = j*100 + 50;
      board[i][j] = 0;
    }
  }
}

void mouseClicked() {
  int spacex=0, spacey=0;
  
  if (turn==1 && !isGameFinished) {
    if (mouseX > 0 && mouseX < 100 && mouseY > 0 && mouseY < 100) {
      spacex = 0;
      spacey = 0;     
    } else if (mouseX > 100 && mouseX < 200 && mouseY > 0 && mouseY < 100) {
      spacex = 1;
      spacey = 0;     
    } else if (mouseX > 200 && mouseX < 300 && mouseY > 0 && mouseY < 100) {
      spacex = 2;
      spacey = 0;
    } else  if (mouseX > 0 && mouseX < 100 && mouseY > 100 && mouseY < 200) {
      spacex = 0;
      spacey = 1;
    } else if (mouseX > 100 && mouseX < 200 && mouseY > 100 && mouseY < 200) {
      spacex = 1;
      spacey = 1;
    } else if (mouseX > 200 && mouseX < 300 && mouseY > 100 && mouseY < 200) {
      spacex = 2;
      spacey = 1;
    } else  if (mouseX > 0 && mouseX < 100 && mouseY > 200 && mouseY < 300) {
      spacex = 0;
      spacey = 2;
    } else if (mouseX > 100 && mouseX < 200 && mouseY > 200 && mouseY < 300) {
      spacex = 1;
      spacey = 2;
    } else if (mouseX > 200 && mouseX < 300 && mouseY > 200 && mouseY < 300) {
      spacex = 2;
      spacey = 2;
    }
    
    if (board[spacex][spacey]==0) {
        turn = -1;
        board[spacex][spacey] = 1;
        
        fill(0, 255, 0);
        ellipse(boardx[spacex][spacey], boardy[spacex][spacey], 20, 20);
 
        spacesLeft -= 1;
    }
  }
}

int minmax_gametree(int turn) {
  int max = -100, min=100;
  
  if (spacesLeft==0) {
    int winner = checkGameOver();
    if (winner==1)
      return -1;
    else if (winner==-1)
      return 1;
    else
      return 0;
  }
  
  for (int i=0; i<3; i++) {
    for (int j=0; j<3; j++) {
      if (board[i][j]==0) {
        board[i][j] = turn;
        spacesLeft--;
        int currentValue = minmax_gametree(turn*(-1));
        if (turn==-1 && currentValue>max) {
          max = currentValue;
        } else if (turn==1 && currentValue<min) {
          min = currentValue;
        }
        board[i][j] = 0;
        spacesLeft++;
      }
    }
  }
  
  if (spacesLeft==1) {
    
  }
  
  if (turn==-1)
    return max;
  else
    return min;
}

void draw() {
  int winner, futureWinner, spacex=-11, spacey=-11, max=-100, currentValue;
  
  if (!isGameFinished) {
    winner = checkGameOver();
    if (spacesLeft == 0 || winner == -1 || winner == 1) {
      isGameFinished = true;
      textFont(f,16); 
      fill(0); 
      if (winner==1)
        text("You won!!!", 130, 130);
      else if (winner==-1)
        text("You lost!!!", 130, 130);
      else
        text("Draw!!!", 130, 130);
    }         
  }
    
  if (turn==-1 && !isGameFinished) {  
    boolean done=false;
    // check if wins
    for (int i=0; i<3; i++) {
      for (int j=0; j<3; j++) {
        if (board[i][j]==0) {
          board[i][j] = -1;
          futureWinner = checkGameOver();
          if (futureWinner == -1) {
            println("try to win");
            done = true;
            spacex = i;
            spacey = j;
            break;
          } else {
            board[i][j] = 0;
          }
        }
      }
      if (done)
        break;
    }
    
    if (!done) {
      // prevent loss
      for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
          if (board[i][j]==0) {
            board[i][j] = 1;
            futureWinner = checkGameOver();
            if (futureWinner == 1) {
              println("try to prevent");

              board[i][j] = -1;
              spacex = i;
              spacey = j;
              done = true;
              break;
            }
            
            board[i][j] = 0;
          }
        }
        if (done)
          break;
      }
    }
    
    if (!done) {
      // Game Tree (retrograde analysis) with minmax     
      println("game tree");
      for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
          if (board[i][j]==0) {
            board[i][j] = -1;
            spacesLeft--;
            currentValue = minmax_gametree(1);
            if (currentValue>max) {
              max = currentValue;
              spacex = i;
              spacey = j;
            }
            board[i][j] = 0;
            spacesLeft++;
          }
        }
      }
    }
    
    fill(255, 0, 0);
    ellipse(boardx[spacex][spacey], boardy[spacex][spacey], 20, 20);
    board[spacex][spacey] = -1;
  
    turn = 1;
    spacesLeft -= 1;
  }  
}

int checkGameOver() {
  int current_winner = -10;
  
  if (board[0][0] == board[1][0] && board[1][0] == board[2][0] && board[0][0] == 1) {
    current_winner = 1;
  } else if (board[0][1] == board[1][1] && board[1][1] == board[2][1] && board[0][1] == 1) {
    current_winner = 1;
  } else if (board[0][2] == board[1][2] && board[1][2] == board[2][2] && board[0][2] == 1) {
    current_winner = 1;
  } else if (board[0][0] == board[0][1] && board[0][1] == board[0][2] && board[0][0] == 1) {
    current_winner = 1;
  } else if (board[1][0] == board[1][1] && board[1][1] == board[1][2] && board[1][0] == 1) {
    current_winner = 1;
  } else if (board[2][0] == board[2][1] && board[2][1] == board[2][2] && board[2][0] == 1) {
    current_winner = 1;
  } else if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] == 1) {
    current_winner = 1;
  } else if (board[2][0] == board[1][1] && board[1][1] == board[0][2] && board[2][0] == 1) {
    current_winner = 1;
  } if (board[0][0] == board[1][0] && board[1][0] == board[2][0] && board[0][0] == -1) {
    current_winner = -1;
  } else if (board[0][1] == board[1][1] && board[1][1] == board[2][1] && board[0][1] == -1) {
    current_winner = -1;
  } else if (board[0][2] == board[1][2] && board[1][2] == board[2][2] && board[0][2] == -1) {
    current_winner = -1;
  } else if (board[0][0] == board[0][1] && board[0][1] == board[0][2] && board[0][0] == -1) {
    current_winner = -1;
  } else if (board[1][0] == board[1][1] && board[1][1] == board[1][2] && board[1][0] == -1) {
    current_winner = -1;
  } else if (board[2][0] == board[2][1] && board[2][1] == board[2][2] && board[2][0] == -1) {
    current_winner = -1;
  } else if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] == -1) {
    current_winner = -1;
  } else if (board[2][0] == board[1][1] && board[1][1] == board[0][2] && board[2][0] == -1) {
    current_winner = -1;
  }
    
  return current_winner;
}
