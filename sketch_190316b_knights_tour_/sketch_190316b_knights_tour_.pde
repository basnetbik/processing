int n=1;
int windowSize = 800;
int center = 400;
boolean stop = false;
int boardSize = 10;
int x = 3, y = 3;

PFont f;


void setup() {
  size(800, 800);

  setUpBoard();
  f = createFont("Arial",16,true); 
}

void draw() {
  if (stop==false)
  {
    variablePoint(); 
  }
}

int[][] boardx = new int [boardSize][boardSize];
int[][] boardy = new int [boardSize][boardSize];
int startx, starty;

void setUpBoard()
{
  background(33,55,33);
  
  int cellSize = windowSize/boardSize;
  for (int i=0; i<windowSize; i=i+cellSize)
  {
     line(i,0,i,windowSize);
     line(0, i, windowSize, i);
  }
  
  for (int i=0; i<boardSize; i++)
  {
    for (int j=0; j<boardSize; j++)
    {
      boardx[i][j] = i*cellSize + cellSize/2;
      boardy[i][j] = j*cellSize + cellSize/2;
    }
  }
  
  startx = boardx[x][y];
  starty = boardy[x][y];
}

int[][] spaceOccupied = new int[boardSize][boardSize];

int[][] possibleSpaces1 = {{1, 2, 2, 1, -1, -2, -2, -1}, {2, 1, -1, -2, -2, -1, 1, 2}};
int[][] possibleSpaces2 = {{-2, -1, 1, 2, 2, 1, -1, -2}, {1, 2, 2, 1, -1, -2, -2, -1}};
int[][] possibleSpaces3 = {{-1, -2, -2, -1, 1, 2, 2, 1}, {-2, -1, 1, 2, 2, 1, -1, -2}};
int[][] possibleSpaces4 = {{2, 1, -1, -2, -2, -1, 1, 2}, {-1, -2, -2, -1, 1, 2, 2, 1}};


void possibleSpaces(int[][] pSpaces)
{  
  for (int i=0; i<8; i++)
  {
    int newx = x+pSpaces[0][i];
    int newy = y+pSpaces[1][i];
    
    if ( newx>=0 && newx < boardSize && newy >=0 && newy < boardSize && spaceOccupied[newx][newy]==0) 
    {  
      x = newx;
      y = newy;
      break;
    }
  }
  
}

int[] RGB = new int [3];
void createRGB()
{
  RGB[0] = int(random(100, 256));
  RGB[1] = int(random(100, 256));
  RGB[2] = int(random(100, 256));
}

void variablePoint() {
  if (n<=boardSize*boardSize)
  {
    createRGB();
    if (n>0)
    {
      stroke(RGB[0], RGB[1], RGB[2]);
      line(startx, starty, boardx[x][y], boardy[x][y]);
      println(startx, starty);
      startx = boardx[x][y];
      starty = boardy[x][y];
    }
    
    textFont(f, 20); 
    fill(RGB[0], RGB[1], RGB[2]); 
    text(n, boardx[x][y], boardy[x][y]);
    spaceOccupied[x][y] = 1;
    
    int currentx = boardx[x][y]-center;
    int currenty = boardy[x][y]-center;
    
    if (currentx > 0 && currenty > 0)
      possibleSpaces(possibleSpaces1);
    else if (currentx < 0 && currenty > 0)
      possibleSpaces(possibleSpaces2);
    else if (currentx < 0 && currenty < 0)
      possibleSpaces(possibleSpaces3);
    else
      possibleSpaces(possibleSpaces4);
    
    n ++;
  }
  else {
    stop = true;
    saveFrame();
  }
}
