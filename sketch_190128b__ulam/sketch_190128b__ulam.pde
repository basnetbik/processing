int n=1;
int windowSize = 800;
int x=windowSize/2;
int y=windowSize/2;
boolean[] isPrime = new boolean [windowSize*windowSize+1];
int round = 0;
boolean stop = false;

void setup() {
  size(800, 800);
  background(0);
  createPrimeChecker();
}

void draw() {
  if (stop==false)
  {
    if (round==0)
      variablePoint();
    else
      variablePointRest();  
  }
  
}

void createPrimeChecker()
{
  for (int i=1; i<=windowSize*windowSize; i++)
  {
     isPrime[i] = true;
  }
  
  isPrime[1] = false;
  
  for (int nn=2; nn<sqrt(windowSize*windowSize); nn++)
  {
    for (int i=nn+1; i<=windowSize*windowSize; i++)
    {
      if (isPrime[i] == true && i%nn==0)
      {
        isPrime[i] = false;
      }
    }
  }
}

int[] RGB = new int [3];
void createRGB()
{
  RGB[0] = int(random(100, 256));
  RGB[1] = int(random(100, 256));
  RGB[2] = int(random(100, 256));
}

int k = 1;
int cx = 1;
int cy = -1;
ArrayList logx =  new ArrayList();
ArrayList logy =  new ArrayList();

void variablePoint() {
  if (n<=windowSize*windowSize)
  {
    for (int j=0; j<k; j++) {   
      if (isPrime[n] == true) {
        createRGB();
        stroke(RGB[0],RGB[1],RGB[2]);
        point(x, y);
        logx.add((Integer)x);
        logy.add((Integer)y);
      }
  
      x = x+(1*cx);
      n = n+1;
      if (n>windowSize*windowSize)
        break;
    }
    cx = cx*(-1);
   
   if (n<=windowSize*windowSize)
   {
     for (int j=0; j<k; j++) {
      if (isPrime[n] == true) {
        createRGB();
        stroke(RGB[0],RGB[1],RGB[2]);
        point(x, y);
        logx.add((Integer)x);
        logy.add((Integer)y);
      } 
      
      y = y+(1*cy);
      n = n+1;
      if (n>windowSize*windowSize)
        break;
    }
    cy = cy*(-1);
   }
   
    k = k+1;
  }
  else {
    round = 1;
    saveFrame();
    background(0);
  }
}

int j = 1;
ArrayList newlogx =  new ArrayList();
ArrayList newlogy =  new ArrayList();

void variablePointRest()
{
  if (round%2!=0)
  {
    if (j<=logx.size())
    {
      if (isPrime[j] == true) {  
        int x = (int) logx.get(j-1);
        int y = (int) logy.get(j-1);
  
        createRGB();
        stroke(RGB[0],RGB[1],RGB[2]);
        point(x, y);
        newlogx.add(x);
        newlogy.add(y);
      }
      j++;
    }
    else
    {
      round++;
      saveFrame();
      logx = new ArrayList();
      logy = new ArrayList();
      j = 1;
      background(0);
      if (newlogx.size() == 0)
        stop = true;
    }
  }
  else
  {
    if (j<=newlogx.size())
    {
      if (isPrime[j] == true) {      
        int x = (int) newlogx.get(j-1);
        int y = (int) newlogy.get(j-1);
  
        createRGB();
        stroke(RGB[0],RGB[1],RGB[2]);
        point(x, y);
        logx.add(x);
        logy.add(y);
      }
      j++;
    }
    else
    {
      round++;
      saveFrame();
      newlogx = new ArrayList();
      newlogy = new ArrayList();
      j = 1;
      background(0);
      if (logx.size() == 0)
        stop = true;
    }
  }
}
