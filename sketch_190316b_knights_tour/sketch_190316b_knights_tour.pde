int n=1;
int windowSize = 800;
int center = 400;
boolean stop = false;

//int x = 0, y = 4;
//int x = 3, y = 0;
//int x = 4, y = 7;
//int x = 7, y = 3;
int x = 3, y = 0;

PFont f;


void setup() {
  size(800, 800);

  setUpBoard();
  f = createFont("Arial",16,true); 
}

void draw() {
  if (stop==false)
  {
    variablePoint(); 
  }
}

int[][] boardx = new int [8][8];
int[][] boardy = new int [8][8];

void setUpBoard()
{
  background(33,55,33);
  line(100,0,100,800);
  line(200,0,200,800);
  line(300,0,300,800);
  line(400,0,400,800);
  line(500,0,500,800);
  line(600,0,600,800);
  line(700,0,700,800);
  line(0,100,800,100);
  line(0,200,800,200);
  line(0,300,800,300);
  line(0,400,800,400);
  line(0,500,800,500);
  line(0,600,800,600);
  line(0,700,800,700);
  
  for (int i=0; i<8; i++)
  {
    for (int j=0; j<8; j++)
    {
      boardx[i][j] = i*100 + 50;
      boardy[i][j] = j*100 + 50;
    }
  }
}

int[][] spaceOccupied = new int[8][8];

int[][] possibleSpaces1 = {{1, 2, 2, 1, -1, -2, -2, -1}, {2, 1, -1, -2, -2, -1, 1, 2}};
int[][] possibleSpaces2 = {{-2, -1, 1, 2, 2, 1, -1, -2}, {1, 2, 2, 1, -1, -2, -2, -1}};
int[][] possibleSpaces3 = {{-1, -2, -2, -1, 1, 2, 2, 1}, {-2, -1, 1, 2, 2, 1, -1, -2}};
int[][] possibleSpaces4 = {{2, 1, -1, -2, -2, -1, 1, 2}, {-1, -2, -2, -1, 1, 2, 2, 1}};


void possibleSpaces(int[][] pSpaces)
{  
  for (int i=0; i<8; i++)
  {
    int newx = x+pSpaces[0][i];
    int newy = y+pSpaces[1][i];
    
    if ( newx>=0 && newx < 8 && newy >=0 && newy < 8 && spaceOccupied[newx][newy]==0) 
    {  
      x = newx;
      y = newy;
      break;
    }
  }
  
}

void variablePoint() {
  if (n<=64)
  {
    textFont(f,16); 
    fill(0); 
    text(n, boardx[x][y], boardy[x][y]);
    spaceOccupied[x][y] = 1;
    
    int currentx = boardx[x][y]-center;
    int currenty = boardy[x][y]-center;
    
    if (currentx > 0 && currenty > 0)
      possibleSpaces(possibleSpaces1);
    else if (currentx < 0 && currenty > 0)
      possibleSpaces(possibleSpaces2);
    else if (currentx < 0 && currenty < 0)
      possibleSpaces(possibleSpaces3);
    else
      possibleSpaces(possibleSpaces4);
    
    n ++;
  }
  else {
    stop = true;
    saveFrame();
  }
}
