int x=0;
int y=0;
int done = 0;
int threads = 0;

int windowHeight = 800; // divisible by 10
int windowWidth = 800;
int threadCount = 800/10;

PImage img = createImage(windowWidth, windowHeight, RGB);
PFont f;


void setup() {
  size(800, 800);
  background(0, 255, 0);
  f = createFont("Arial",16,true); 
  
  textFont(f, 20); 
  fill(255, 255, 255); 
  text("Wait a moment!! The image is being generated.", 33, 333);
  
  for (int i = 0; i < threadCount; i++) {
    thread("partialVariablePoint");
    delay(10);
  }
}

void draw() { 
  if (done==threadCount) {
    img.save("blue_yellow.jpg");
    delay(1000);
    done ++;
    image(img, 0, 0);
  }
}

color blue = color(0, 0, 255);
color yellow = color(255, 255, 0);

void partialVariablePoint() {
  int currentRow = threads*10;
  threads ++;
  
  for (int y=currentRow; y<currentRow+10; y++) {
    for (int x=0; x<windowWidth; x++) {
      if ((x+y)%2 == 0) {
        img.set(x, y, blue);
      } else {
        img.set(x, y, yellow);
      }
    }
  }
  
  done ++;
}
