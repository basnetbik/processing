color violet = color(127, 0, 255, 150);
color indigo = color(63, 0, 255, 150);
color blue = color(0, 0, 255, 150);
color green = color(0, 255, 0, 150);
color yellow = color(255, 255, 0, 150);
color orange = color(255, 127, 0, 150);
color red = color(255, 0, 0, 150);

// Polar coordinate system
float r_v = 0;
float theta_v = 0;
float r_i = 0;
float theta_i = 51;
float r_b = 0;
float theta_b = 102;
float r_g = 0;
float theta_g = 153;
float r_y = 0;
float theta_y = 204;
float r_o = 0;
float theta_o = 255;
float r_r = 0;
float theta_r = 306;

void setup() {
  size(1000, 800);
  background(100);
}

void draw() {
  // Polar to Cartesian conversion
  float x_v = r_v * cos(theta_v);
  float y_v = r_v * sin(theta_v);
  float x_i = r_i * cos(theta_i);
  float y_i = r_i * sin(theta_i);
  float x_b = r_b * cos(theta_b);
  float y_b = r_b * sin(theta_b);
  float x_g = r_g * cos(theta_g);
  float y_g = r_g * sin(theta_g);
  float x_y = r_y * cos(theta_y);
  float y_y = r_y * sin(theta_y);  
  float x_o = r_o * cos(theta_o);
  float y_o = r_o * sin(theta_o);
  float x_r = r_r * cos(theta_r);
  float y_r = r_r * sin(theta_r);

  // Draw ellipses at xs, ys
  noStroke();
  
  fill(violet);
  // Adjust for center of window
  ellipse(x_v+width/2, y_v+height/2, 3, 3);
  
  fill(indigo);
  // Adjust for center of window
  ellipse(x_i+width/2, y_i+height/2, 3, 3); 

  fill(blue);
  // Adjust for center of window
  ellipse(x_b+width/2, y_b+height/2, 3, 3); 

  fill(green);
  // Adjust for center of window
  ellipse(x_g+width/2, y_g+height/2, 3, 3); 

  fill(yellow);
  // Adjust for center of window
  ellipse(x_y+width/2, y_y+height/2, 3, 3); 

  fill(orange);
  // Adjust for center of window
  ellipse(x_o+width/2, y_o+height/2, 3, 3); 
  
  fill(red);
  // Adjust for center of window
  ellipse(x_r+width/2, y_r+height/2, 3, 3); 


  // Increment the angle
  theta_v += 0.07;
  theta_i += 0.07;
  theta_b += 0.07;
  theta_g += 0.07;
  theta_y -= 0.07;
  theta_o -= 0.07;
  theta_r -= 0.07;
  // Increment the radius
  r_v += 0.1;
  r_i += 0.1;
  r_b += 0.1;
  r_g += 0.1;
  r_y += 0.1;
  r_o += 0.1;
  r_r += 0.1;
}
