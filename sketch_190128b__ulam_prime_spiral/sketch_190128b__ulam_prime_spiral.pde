int n=1;
int windowSize = 800;
int x=windowSize/2;
int y=windowSize/2;
boolean[] isPrime = new boolean [windowSize*windowSize+1];
boolean stop = false;
int round = 0;

void setup() {
  size(800, 800);
  background(33,55,33);
  createPrimeChecker();
}

void draw() {
  if (stop==false)
  {
    variablePoint(); 
  }
}

void createPrimeChecker()
{
  for (int i=1; i<=windowSize*windowSize; i++)
  {
     isPrime[i] = true;
  }
  
  isPrime[1] = false;
  
  for (int nn=2; nn<sqrt(windowSize*windowSize); nn++)
  {
    for (int i=nn+1; i<=windowSize*windowSize; i++)
    {
      if (isPrime[i] == true && i%nn==0)
      {
        isPrime[i] = false;
      }
    }
  }
}

int[] RGB = new int [3];
void createRGB()
{
  RGB[0] = int(random(100, 256));
  RGB[1] = int(random(100, 256));
  RGB[2] = int(random(100, 256));
}

int k = 1;
int cx = 1;
int cy = -1;

void next(int direction) {
  for (int j=0; j<k; j++) {   
    if (isPrime[n] == true) {
      createRGB();
      stroke(RGB[0], RGB[1], RGB[2]);
      point(x, y);
    }
    
    if (direction==0)  
      x = x+(1*cx);
    else
      y = y+(1*cy);
    
    n = n+1;
    if (n>windowSize*windowSize)
      break;
  }
  
  if (direction==0)  
    cx = cx*(-1);
  else
    cy = cy*(-1);
}

void variablePoint() {
  if (n<=windowSize*windowSize)
  {
    next(0);
   
    if (n<=windowSize*windowSize)
    {
      next(90);
      k = k+1;
    }
  }
  else {
    stop = true;
    saveFrame();
  }
}
