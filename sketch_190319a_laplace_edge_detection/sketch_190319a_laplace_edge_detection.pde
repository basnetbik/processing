PImage photo;
String filename = "goldenspiral.png";
int cutOffValue = 33;  // this value might be different for each image for better edge detection


void setup() {
  size(1782, 1690);
  photo = loadImage(filename);
  detect_edge();
}

void draw(){}

void detect_edge() {
  PImage img = createImage(photo.width, photo.height, RGB);
  
  for (int i=0; i<photo.width; i++)
  {
    for (int j=0; j<photo.height; j++)
    {
      int xFuture = i+1;
      int xPast = i-1;
      int yFuture = j+1;
      int yPast = j-1;
      
      if (xFuture>=photo.width)
        xFuture = photo.width - 2;
      else if (xPast<0)
        xPast = 1;
      if (yFuture>=photo.height)
        yFuture = photo.height - 2;
      else if (yPast<0)
        yPast = 1;
      
      color c = photo.get(i, j);
      float LumaCurrent = 0.299*red(c) + 0.587*green(c) + 0.114*blue(c);
      
      c = photo.get(i+1, j);
      float LumaFutureX = 0.299*red(c) + 0.587*green(c) + 0.114*blue(c);
      c = photo.get(i-1, j);
      float LumaPastX = 0.299*red(c) + 0.587*green(c) + 0.114*blue(c);
      c = photo.get(i, j+1);
      float LumaFutureY = 0.299*red(c) + 0.587*green(c) + 0.114*blue(c);
      c = photo.get(i, j-1);
      float LumaPastY = 0.299*red(c) + 0.587*green(c) + 0.114*blue(c);
      
      float partialSecondDerivativeX = LumaFutureX - 2*LumaCurrent + LumaPastX;
      float partialSecondDerivativeY = LumaFutureY - 2*LumaCurrent + LumaPastY;
      
      float selfGradient = partialSecondDerivativeX + partialSecondDerivativeY;
      
      
      if (selfGradient > cutOffValue)
      {
        img.set(i, j, color(255)); 
        // println(selfGradient);
      }
      else 
        img.set(i, j, color(0));
    }
  }
  
  image(img, 0, 0);
  img.save("laplace_edge_"+filename);
}
